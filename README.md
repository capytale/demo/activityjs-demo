# Demo du paquet @capytale/activity.js
Le paquet [@capytale/activity.js](https://forge.aeif.fr/capytale/activity-js) offre une API qui permet à un player d’interagir avec le backend Capytale.

Le *player* est l'outil qui permet à l'utilisateur de visualiser et modifier une activité.
Une *activité* au sens Capytale peut être :
- une activité créée par un enseignant (un *ActivityNode*)
- une copie d'élève associée à une activité enseignant (un *AssignmentNode* associé à un *ActivityNode*)

Une *activité* est identifiée par un numéro, son *id*. Un *id* peut aussi bien correspondre à une activité enseignant qu'à une copie élève.  
Actuellement, le player n'a pas l'initiative de créer les nouvelles *activités*. Il reçoit en général l'*id* de l'*activité* sous la forme d'un paramètre d'URL ?id=...

Le type d'une activité est identifié par un nom système. Par exemple "notebook.python", "notebook.sql", "html", "pyxelstudio", etc... Ce type est transmis comme un champ du *ActivityNode*.
## point d'entrée de l'API
Le point d'entrée de l'API est la fonction asynchrone
```typescript
import load from "@capytale/activity.js/backend/capytale/activitySession";
```
qui a la signature suivante
```typescript
function load(id: number): Promise<ActivitySession>;
```
Cette fonction contacte le backend pour récupérer les informations nécessaires et renvoie un objet *ActivitySession*
## l'objet *ActivitySession*
```typescript
class ActivitySession {
    me: Me // Un objet avec des informations sur l'utilisateur connecté
    activityType: string // Le nom système du type d'activité
    type: ActivityType // Un objet avec des informations sur le type d'activité (nom système, icône, etc...)
    returnUrl: string // L'URL de retour à utiliser si l'utilisateur quitte le player
    isAssignment: boolean // true s'il s'agit d'une copie élève, false s'il s'agit d'une activité enseignant
    mode: "assignment" | "create" | "review" | "view" | "detached"
    activityBunch: ActivityBunch // cf. infra
    ...
}
```
> Le nom *ActivitySession* indique que les données présentées par cet objet dépendent de l'utilisateur connecté.

Les mode :
- "assignment" : un élève visualise sa copie (implique *isAssignment* vaut true)
- "create" : un enseignant visualise une activité enseignant
- "review" : un enseignant visualise la copie d'un élève (implique *isAssignment* vaut true)
- "view" : un utilisateur visualise une activité enseignant partagée dans la bibliothèque (donc en lecture seule)
- "detached" : player non lié au backend (actuellement inutilisé)

Le contenu proprement dit de l'*activité* est accessible par l'objet *ActivityBunch*.
## l'objet *ActivityBunch*
> Cet objet expose une abstraction pour accéder :
> - à un *ActivityNode* seul dans le cas d'une activité enseignant,
> - ou à un *AssignmentNode* associé à un *ActivityNode* dans le cas d'une copie élève.
> 
> Le nom *ActivityBunch* indique que cet objet résulte de la combinaison d'un ou deux objets du backend.

L'API intègre un mécanisme pour que la classe utilisée pour instancier l'objet *ActivityBunch* soit choisie en fonction du type de l'activité qui a été chargée.  
Par ce mécanisme, il est possible d'implémenter des fonctionnalités spécifiques dans la classe *ActivityBunch*.  
Déclarer plusieurs classes *ActivityBunch* n'est utile que si le player est prévu pour gérer plusieurs types d'activité. Dans la pratique, on se contente de choisir une classe parmi celles fournies dans @capytale/activity.js et de la déclarer comme classe par défaut. 

Par exemple, pour une activité avec un contenu (éventuellement binaire) stocké en backend dans le système de fichier plutôt qu'en base, on choisira un *FsActivityBunch*
```typescript
import FsActivityBunch from "@capytale/activity.js/activity/activityBunch/base/fs";
FsActivityBunch.declareFor(); // Déclaration de cette classe comme classe par défaut.
// Ainsi, l'API instanciera systématiquement un FsActivityBunch lors de l'appel de load(id)
```
## l'objet *FsActivityBunch*
```typescript
class FsActivityBunch {
    // Les entités backend
    activityNode: ActivityNode
    assignmentNode: AssignmentNode | null
    mainNode: ActivityNode | AssignmentNode

    // Les champs
    title: WritableSingleValueField<string> // Titre de l'activité
    attached_files: MultiValueField<string> // URL des fichiers joints à l'activité
    ...

    content: WritableFSField // Le contenu (par exemple le notebook)

    // Gestion de l'état par rapport au backend
    isDirty: boolean
    save(): Promise<void>
}
```

> L'accès aux champs se fait à travers des objects *Field*. cf. infra.

> Cette version de *ActivityBunch* intègre la logique du contenu élève basé sur le contenu prof dans le cas d'une copie d'élève:
> - à la lecture d'une nouvelle copie, le champ *content* présentera le contenu lu dans le *ActivityNode*,
> - à l'écriture, le contenu sera écrit dans le *AssignmentNode*,
> - à la lecture suivante, le champ *content* présentera le contenu lu dans le *AssignmentNode*.

## Les objets *Field*

> Les objets *Field* existent en plusieurs implémentations selon 
> - qu'ils sont uni-valués ou multi-valués,
> - read only ou read/write,
> - selon le type des données.

> Ils offrent deux mécanismes de modification :
> - une simple affectation de la nouvelle valeur,
> - ou le câblage d'une fonction source (qui peut être asynchrone) puis l'invocation de la méthode `invalidate()` pour signifier que la valeur a changé.

Par exemple
```typescript
class WritableFSField {
    getAsync<T extends ReturnType>(type: T = 'text'): Promise<ReturnValueType<T>|null>
    set value(v: any | null)
    setSource(s?: () => any): void
    invalidate(): void
}
```

