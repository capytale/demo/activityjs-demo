import meApi from "@capytale/activity.js/backend/capytale/me";
import clock from "@capytale/activity.js/backend/capytale/clock";
import httpClient from "@capytale/activity.js/backend/capytale/http";
import tracker from "@capytale/activity.js/backend/capytale/tracker";
import rest from "@capytale/activity.js/backend/capytale/rest";
import fsFields from "@capytale/activity.js/backend/capytale/fs";
import urlParser from "@capytale/activity.js/backend/capytale/urlParser";

(function () {
    (globalThis as any).me = meApi;
    (globalThis as any).clock = clock;
    (globalThis as any).httpClient = httpClient;

    const note_ctl = document.getElementById('note') as HTMLTextAreaElement;
    const note = {
        addLn(t: string = '') {
            this.add(t)
            this.add('\n')
        },
        add(t: string = '') {
            note_ctl.value += t;
        },
        clear() {
            note_ctl.value = '';
        }
    };


    (async () => {
        note.addLn('Server time : ' + await clock.now() + ' (appel api pour synchonisation au plus toutes les 2 minutes)');
        note.addLn();

        note.addLn('Tracking: tout est auto configuré (siteId & muuid)');
        tracker.trackPageView();
        // et aussi tracker.push() s'utilise comme _paq.push() de matomo
        // ex. tracker.push(['trackEvent', 'test', 'test']);
        note.addLn();

        // Le tracker a déjà demandé me donc ici, le cache est renvoyé.
        const me = await meApi.getMeAsync();
        note.addLn(`Utilisateur courant : ${me?.lastname} (uid = ${me?.uid})`);
        console.log('me:', me);
        // Pour vider le cache:
        // meApi.clearMe();
        note.addLn();

        const nid = urlParser.getActivityNid();
        if (nid != null) {
            const mode = urlParser.getActivityMode();
            note.addLn('nid: ' + nid + '\nmode: ' + mode);

            const ref = { type: 'node', id: nid };
            try {
                const data = await rest.getAsync(ref);
                note.addLn('Activité: ' + data.title[0].value);
                const ownerRef = { type: 'user', id: data.uid[0].target_id };
                try {
                    const owner = await rest.getAsync(ownerRef);
                    note.addLn(`Propriétaire: ${owner.field_nom[0].value} (uid = ${owner.uid[0].value})`);
                } catch (error) {
                    note.addLn('Erreur en chargeant le propriétaire de l\'activité : ' + error.message);
                }

            }
            catch (error) {
                note.addLn('Erreur en chargeant l\'activité : ' + error.message);
            }
            note.addLn();

            try {
                const content = await fsFields.getFieldAsync(ref, 'content');
                if (content == null)
                    note.addLn('Le fs field Content est vide');
                else
                    note.addLn('Le Fs field Content fait ' + content.length + ' caractères');
            }
            catch (error) {
                note.addLn('Erreur: l\'activité n\'a probablement pas de fsField : ' + error.message);
            }
            // Autres formats possible :
            // const ab = await fsFields.getFieldAsync(ref, 'content', 'arrayBuffer');
            // const b = await fsFields.getFieldAsync(ref, 'content', 'blob');
            // const s = await fsFields.getFieldAsync(ref, 'content', 'stream');
            // const j = await fsFields.getFieldAsync(ref, 'content', 'json');



        }
    })();
})();
